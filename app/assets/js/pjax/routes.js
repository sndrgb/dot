import config from './config';

module.exports = {
    [`${config.BASE}`]: {
        section: require('../sections/products'),
        duplicate: true,
    },
    [`${config.BASE}en`]: {
        section: require('../sections/products'),
        duplicate: true,
    },

    [`${config.BASE}contact`]: {
        section: require('../sections/contact'),
        duplicate: true,
    },
    [`${config.BASE}en/contact`]: {
        section: require('../sections/contact'),
        duplicate: true,
    },

    [`${config.BASE}about`]: {
        section: require('../sections/about'),
        duplicate: true,
    },

    [`${config.BASE}en/about`]: {
        section: require('../sections/about'),
        duplicate: true,
    },

    [`${config.BASE}tss`]: {
        section: require('../sections/tss'),
        duplicate: true,
    },

    [`${config.BASE}en/tss`]: {
        section: require('../sections/tss'),
        duplicate: true,
    },

    [`${config.BASE}:id`]: {
        section: require('../sections/product'),
        duplicate: true,
    },
    [`${config.BASE}en/:id`]: {
        section: require('../sections/product'),
        duplicate: true,
    },
};
