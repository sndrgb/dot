const config = {

    BASE: '/',
    ENTRY: '/',
    LANG: 'it',

    $body: document.body,
    $view: document.querySelector('#js-page'),

    width: window.innerWidth,
    height: window.innerHeight,

    isMobile: false,
};

export default config;
