import classes from 'dom-helpers/class';
import create from 'dom-create-element';
import 'whatwg-fetch';
import framework from './framework';
import config from './config';

const utils = {
    addRoutingEL(a) {
        a.forEach((el) => {
            el.onclick = utils.handleRoute;
        });
    },

    removeRoutingEL(a) {
        a.forEach((el) => {
            el.onclick = null;
        });
    },

    handleRoute(e) {
        const target = e.currentTarget;

        if (classes.hasClass(target, 'no-route') || (target.hasAttribute('target') && target.getAttribute('target') === '_blank')) {
            return;
        }

        e.preventDefault();
        framework.go(target.getAttribute('href'));
    },

    getSlug(req, options) {
        const params = Object.keys(req.params).length === 0 && JSON.stringify(req.params) === JSON.stringify({});
        let route = req.route === config.BASE ? config.ENTRY : req.route;

        if (!params) {
            for (const key in req.params) {
                if (req.params.hasOwnProperty(key)) {
                    if (route.indexOf(key) > -1) {
                        route = route.replace(`:${key}`, options.sub ? '' : req.params[key]);
                    }
                }
            }
        }

        if (route.substring(route.length - 1) === '/') {
            route = route.slice(0, -1);
        }

        return route.substr(1);
    },

    createPage(req, slug) {
        const style = (slug === '') ? 'products' : slug;

        return create({
            selector: 'section',
            id: `page-${style.replace('/', '-')}`,
            styles: `page page-${style.replace('/', '-')}`,
        });
    },

    loadPage(req, view, done, options) {
        const slug = this.getSlug(req, options);
        const page = this.createPage(req, slug);
        const data = JSON.parse(window._data);

        fetch(`/${slug}`).then(res => res.text()).then((object) => {
            const doc = document.implementation.createHTMLDocument();
            let rendered;
            const lang = localStorage.getItem('language') || 'it';

            define(['nunjucks'], (nunjucks) => {
                rendered = nunjucks.renderString(object, data[lang]);
            });

            doc.documentElement.innerHTML = rendered;
            const inner = doc.querySelector('[data-content]');
            page.appendChild(inner);

            done();
        });

        return view.appendChild(page);
    },

    switchLanguage(e, lang) {
        e.preventDefault();
        localStorage.setItem('language', lang);
        window.location.reload(true);
    },
};

export default utils;
