export const qsa = (selector, ctx = document) => Array.from(ctx.querySelectorAll(selector));
export const qs = (selector, ctx = document) => ctx.querySelector(selector);

export function handleEvent(eventName, {onElement, withCallback, useCapture = false} = {}, thisArg) {

    const element = onElement || document.documentElement;

    function handler(event) {
        if (typeof withCallback === 'function') {
            withCallback.call(thisArg, event);
        }
    }

    handler.destroy = function () {
        return element.removeEventListener(eventName, handler, useCapture);
    };

    element.addEventListener(eventName, handler, useCapture);
    return handler;
}

export function createconfig(o = {}, config) {
    return Object.assign({}, config, o);
}

export const stringToDOM = (string = '') => {
    const fragment = document.createDocumentFragment();
    var wrapper = fragment.appendChild(document.createElement('div'));

    wrapper.innerHTML = string.trim();
    return wrapper.children[0];
};

export function index(element) {
    let sib = element.parentNode.childNodes,
        n = 0;

    for (let i = 0; i < sib.length; i++) {
        if (sib[i] === element) return n;
        if (sib[i].nodeType === 1) n++;
    }

    return -1;
}

export function eq(parent, index) {
    return (index >= 0 && index < parent.length) ? parent[index] : -1;
}

export function inViewport(element) {
    let rect = element.getBoundingClientRect(),
        html = document.documentElement;

    return (rect.top >= 0 && rect.top < html.clientHeight) || (rect.bottom >= 0 && rect.bottom < html.clientHeight);

}

export function getDevice() {
    let device = window.getComputedStyle(document.body, '::after').getPropertyValue('content');
    device = device.replace(/('|")/g, '');

    return device;
}

export function isMobile() {
    return !(getDevice() !== 'mobile' && getDevice() !== 's-tablet');
}

export function calculateRatio(width, height) {
    return width / height;
}

export function lerp(a, b, n) {
    return ((1 - n) * a) + (n * b);
}

export function viewportSize() {
    let coords = {
        x: window.innerWidth,
        y: window.innerHeight,
    };

    window.addEventListener('resize', () => {
        coords = {
            x: window.innerWidth,
            y: window.innerHeight,
        };
    });

    return coords;
}
