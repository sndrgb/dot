export const productsConfig = [
    {
        id: 'tampon',
        width: 1246,
        height: 800,
        next: 'panty-liner',
    }, {
        id: 'pad',
        width: 1440,
        height: 585,
        next: 'applicator-tampons',
    }, {
        id: 'liner',
        width: 1570,
        height: 350,
        next: 'night-pads',
    }, {
        id: 'sanity',
        width: 1200,
        height: 460,
        next: 'hygenic-pad',
    },
];

export const aboutConfig = [
    {
        id: 'about-first',
        width: 1920,
        height: 1080,
    },
];

export const tssConfig = [
    {
        id: 'about-second',
        width: 1920,
        height: 1080,
    }
];
