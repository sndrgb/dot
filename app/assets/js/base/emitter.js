import EventEmitter from 'events';

export default new EventEmitter().setMaxListeners(0);