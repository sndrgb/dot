import loop from 'raf-loop';
import gsap from 'gsap';

class Sprite {
    constructor(opts) {
        // Get canvas
        this.canvas = document.getElementById('sprite');
        this.index = 0;
        this.frameIndex = 0;
        this.tickCount = 0;
        this.context = this.canvas.getContext('2d');
        this.loop = loop(this.render.bind(this));
        this.ticksPerFrame = opts.ticks;
        this.numberOfFrames = opts.frames;

        this.images = [];
        this.widths = [];
        this.heights = [];
        this.sprites = [];
        this.singleWidths = [];

        this.isAnimateIn = false;
        this.isAnimateOut = false;
    }


    addSprites(src) {
        const count = src.length;
        let loaded = 0;

        src.forEach((el, i) => {
            const image = new Image();
            image.src = `/assets/images/${el.id}-sprite.png`;

            this.images.push(image);
            this.widths.push(el.width * this.numberOfFrames);
            this.heights.push(el.height);
            this.singleWidths.push(el.width);

            this.canvas.width = el.width;
            this.canvas.height = el.height;

            loaded++;

            if (loaded === count) {
                this.loop.start();
            }
        });
    }

    animateIn(id) {
        this.index = id;
        gsap.set(this.canvas, { display: 'block' });
        this.isAnimateIn = true;
        this.tickCount += 1;

        if (this.tickCount > this.ticksPerFrame) {
            this.tickCount = 0;

            // If the current frame index is in range
            if (this.frameIndex < this.numberOfFrames - 1) {
                this.frameIndex += 1;

                // Draw the animation
                this.fx = Math.floor(this.frameIndex % 6) * this.singleWidths[this.index];
                this.fy = Math.floor(this.frameIndex / 6) * this.heights[this.index];

                this.yPosition = (this.frameIndex * this.widths[this.index]) / this.numberOfFrames;
            } else {
                this.frameIndex = this.numberOfFrames - 1;
                this.isAnimateIn = false;
            }
        }
    }

    animateOut(id) {
        this.index = id;
        this.isAnimateOut = true;
        this.tickCount += 1;

        if (this.tickCount > this.ticksPerFrame) {
            this.tickCount = 0;

            // If the current frame index is in range
            if (this.frameIndex >= 0) {
                this.frameIndex -= 1;

                // Draw the animation
                this.fx = Math.floor(this.frameIndex % 6) * this.singleWidths[id];
                this.fy = Math.floor(this.frameIndex / 6) * this.heights[id];
                // this.yPosition = this.width - (this.frameIndex * this.singleWidth);
            } else {
                this.yPosition = 0;
                gsap.set(this.canvas, { display: 'none' });
                this.isAnimateOut = false;
            }
        }
    }

    switchTo(oldId, newId) {
        this.animateOut(oldId);

        setTimeout(() => {
            this.animateIn(newId);
        }, 1500);
    }

    render() {
        if (this.isAnimateIn) {
            this.animateIn(this.index);
        } else if (this.isAnimateOut) {
            this.animateOut(this.index);
        }

        // Clear the canvas
        this.context.clearRect(0, 0, this.widths[this.index], this.heights[this.index]);
        this.context.drawImage(this.images[this.index], this.fx, this.fy, this.singleWidths[this.index], this.heights[this.index], 0, 0, this.singleWidths[this.index], this.heights[this.index]);
    }

    destroy() {
        this.loop.stop();
    }
}

export default Sprite;
