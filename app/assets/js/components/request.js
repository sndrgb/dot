import gsap from 'gsap';
import debounce from 'lodash/debounce';
import bindAll from 'lodash/bindAll';
import approve from 'approvejs';
import superagent from 'superagent';
import 'whatwg-fetch';
import { qsa, handleEvent } from '../base/utils';
import Component from './component';

class Request extends Component {
    constructor(selector, options) {
        super(selector, options);

        this.ui = Object.assign({}, this.ui, {
            bodies: this.qsa('[data-anim="text"]'),
            titles: this.qsa('[data-anim="split"]'),
        });

        this.handleLinks = [];
        this.splits = [];
        this.tl = new TimelineMax({
            paused: true,
        });

        this.tl.set(this.ui.container, {
            display: 'none',
            clearProps: 'all',
        }).to('#js-page, #background', 0.6, {
            scale: 0.8,
            ease: Expo.easeOut,
        }).fromTo(this.ui.container, 0.8, {
            yPercent: 100,
            display: 'block',
        }, {
            yPercent: 0,
            display: 'block',
            ease: Expo.easeOut,
        }, '-=0.3');

        bindAll(this, ['toggle', 'close']);

        this.setupOverlay();
        this.bind();
    }

    setupOverlay() {
        // setting up contact form
        gsap.set('.c-form__row, [data-message]', {
            autoAlpha: 0,
            yPercent: 50,
        });


        gsap.staggerTo('.c-form__row', 0.6, {
            autoAlpha: 1,
            yPercent: 0,
        }, 0.1);

        const debounced = debounce(this.callAjax.bind(this), 200);
        // setting up contact form
        this.sendMail = handleEvent('click', {
            onElement: document.querySelector('#submit-mail'),
            withCallback: (e) => {
                e.preventDefault();
                debounced();
            },
        }, this);

        this.setState('isOpen', false);

        this.splitTitle = new SplitText(this.ui.titles, { type: 'chars' }).chars;
        gsap.set(this.splitTitle, { autoAlpha: 0, y: 20 });

        this.splitBody = new SplitText(this.ui.bodies, { type: 'words' }).words;
        gsap.set(this.splitBody, { autoAlpha: 0, y: 20 });
        gsap.set(this.ui.container, {
            display: 'none',
        });

        this.button = document.querySelector('[data-open="request"]');
        this.translations = {
            open: this.button.querySelector('[data-switch="open"]'),
            close: this.button.querySelector('[data-switch="close"]'),
        };

        gsap.set(this.translations.close, { autoAlpha: 0 });
    }

    bind() {
        this.handleClick = handleEvent('click', {
            onElement: this.button,
            withCallback: this.toggle,
        });
    }

    bindLinks() {
        const links = qsa('a', document);
        links.forEach((el) => {
            const handle = handleEvent('click', {
                onElement: el,
                withCallback: this.close,
            });

            this.handleLinks.push(handle);
        });
    }

    toggle(e) {
        e.preventDefault();

        if (this.getState('isOpen')) {
            this.close(e);
            this.handleLinks.forEach((el) => {
                el.destroy();
            });
        } else {
            this.open(e);
            this.bindLinks();
        }
    }

    open(e) {
        this.setState('isOpen', true);
        this.tl.play();
        this.toggleTranslation();

        gsap.staggerFromTo(this.splitTitle, 0.3, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            ease: Sine.easeOut,
        }, 0.02);

        gsap.staggerFromTo(this.splitBody, 0.4, {
            autoAlpha: 0,
            y: 10,
        }, {
            autoAlpha: 1,
            y: 0,
            ease: Sine.easeOut,
        }, 0.03);

        gsap.staggerFromTo('.c-form__row', 0.4, {
            autoAlpha: 0,
            y: 10,
        }, {
            autoAlpha: 1,
            y: 0,
            ease: Sine.easeOut,
        }, 0.03);
    }

    close(e) {
        e.preventDefault();

        this.setState('isOpen', false);
        this.toggleTranslation();

        gsap.set(this.translations.open, { autoAlpha: 0 });
        gsap.set(this.translations.close, { autoAlpha: 1 });

        gsap.staggerTo(this.splitTitle, 0.3, {
            autoAlpha: 0,
            y: 20,
            ease: Sine.easeOut,
        }, 0.005);

        gsap.staggerTo(this.splitBody, 0.4, {
            autoAlpha: 0,
            y: 10,
            ease: Sine.easeOut,
        }, 0.005);

        gsap.staggerTo('.c-form__row', 0.4, {
            autoAlpha: 0,
            y: 10,
            ease: Sine.easeOut,
        }, 0.005);

        this.tl.reverse();
    }

    toggleTranslation() {
        let open, close;

        if (this.getState('isOpen')) {
            open = this.translations.open;
            close = this.translations.close;
        } else {
            open = this.translations.close;
            close = this.translations.open;
        }

        gsap.to(open, 0.3, {
            autoAlpha: 0,
            y: -5,
            delay: 0.5,
        });

        gsap.fromTo(close, 0.3, {
            autoAlpha: 0,
            y: 5,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 0.5,
        });
    }

    callAjax() {
        const headers = {};
        const rule = {
            required: true,
        };

        this.data = [{
            name: 'name',
            container: this.ui.container.querySelector('#name'),
            value: this.ui.container.querySelector('#name').value,
            test: rule,
        }, {
            name: 'email',
            container: this.ui.container.querySelector('#email'),
            value: this.ui.container.querySelector('#email').value,
            test: { required: true, email: true },
        }, {
            name: 'phone',
            container: this.ui.container.querySelector('#telephone'),
            value: this.ui.container.querySelector('#telephone').value,
            test: null,
        }, {
            name: 'message',
            container: this.ui.container.querySelector('#message'),
            value: this.ui.container.querySelector('#message').value,
            test: rule,
        }, {
            name: 'privacy',
            container: this.ui.container.querySelector('#privacy-check'),
            value: this.ui.container.querySelector('#privacy-check').checked,
            test: rule,
        }];

        this.data.forEach((obj) => {
            if (obj.test !== null) {
                const result = approve.value(obj.value, obj.test);

                if (!result.approved) {
                    gsap.set(obj.container, {
                        className: '+=is-wrong',
                    });
                } else {
                    gsap.set(obj.container, {
                        className: '-=is-wrong',
                    });

                    Object.assign(headers, {
                        [obj.name]: obj.value,
                    });
                }
            } else {
                Object.assign(headers, {
                    [obj.name]: obj.value,
                });
            }

            if (Object.keys(headers).length === this.data.length) {
                superagent.post('/send.php').set('Content-Type', 'application/x-www-form-urlencoded').send(headers).end((err, res) => {
                    if (res.ok) {
                        gsap.staggerTo('.c-form__row', 0.6, {
                            autoAlpha: 0,
                            yPercent: 50,
                            onComplete: () => {
                                gsap.to('[data-message]', 0.5, {
                                    autoAlpha: 1,
                                    yPercent: 0,
                                });
                            },
                        }, 0.1);
                    }
                });
            }
        });
    }
}

export default Request;
