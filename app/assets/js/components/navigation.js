import gsap from 'gsap';
import bindAll from 'lodash/bindAll';
import { qsa, handleEvent } from '../base/utils';
import Component from './component';

class Menu extends Component {
    constructor(selector, options) {
        super(selector, options);

        this.ui = Object.assign({}, this.ui, {
            bodies: this.qsa('[data-anim="text"]'),
            titles: this.qsa('[data-anim="split"]'),
        });

        this.handleLinks = [];
        this.tl = new TimelineMax({
            paused: true,
        });

        this.tl.set(this.ui.container, {
            display: 'none',
            clearProps: 'all',
        }).to('#js-page, #background', 0.6, {
            scale: 0.8,
            ease: Expo.easeOut,
        }).fromTo(this.ui.container, 0.8, {
            yPercent: 100,
            display: 'block',
        }, {
            yPercent: 0,
            display: 'block',
            ease: Expo.easeOut,
        }, '-=0.3');

        bindAll(this, ['toggle', 'close']);

        this.setupOverlay();
        this.bind();
    }

    setupOverlay() {
        this.setState('isOpen', false);

        this.button = document.querySelector('[data-open="menu"]');
        this.translations = {
            open: this.button.querySelector('[data-switch="open"]'),
            close: this.button.querySelector('[data-switch="close"]'),
        };

        gsap.set(this.translations.close, { autoAlpha: 0 });
    }

    bind() {
        this.handleClick = handleEvent('click', {
            onElement: this.button,
            withCallback: this.toggle,
        });
    }

    bindLinks() {
        const links = qsa('a', document);
        links.forEach((el) => {
            const handle = handleEvent('click', {
                onElement: el,
                withCallback: this.close,
            });

            this.handleLinks.push(handle);
        });
    }

    toggle(e) {
        e.preventDefault();

        if (this.getState('isOpen')) {
            this.close(e);
            this.handleLinks.forEach((el) => {
                el.destroy();
            });
        } else {
            this.open(e);
            this.bindLinks();
        }
    }

    open(e) {
        this.setState('isOpen', true);
        this.tl.play();
        this.toggleTranslation();
    }

    close(e) {
        e.preventDefault();

        this.setState('isOpen', false);
        this.toggleTranslation();

        gsap.set(this.translations.open, { autoAlpha: 0 });
        gsap.set(this.translations.close, { autoAlpha: 1 });

        this.tl.reverse();
    }

    toggleTranslation() {
        let open, close;

        if (this.getState('isOpen')) {
            open = this.translations.open;
            close = this.translations.close;
        } else {
            open = this.translations.close;
            close = this.translations.open;
        }

        gsap.to(open, 0.3, {
            autoAlpha: 0,
            y: -5,
            delay: 0.5,
        });

        gsap.fromTo(close, 0.3, {
            autoAlpha: 0,
            y: 5,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 0.5,
        });
    }
}

export default Menu;
