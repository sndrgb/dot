import classes from 'dom-helpers/class';
import gsap from 'gsap';
import emitter from '../base/emitter';
import { handleEvent } from '../base/utils';
import Component from './component';
import Scrolljack from './scrolljack';
import Sprite from '../components/sprite';

class AboutSlideshow extends Component {
    constructor(selector, opts) {
        super(selector, opts);

        this.selector = selector;

        // una lista di nodi non è un array *****
        this.ui = Object.assign({}, this.ui, {
            slides: this.qsa('.c-slide'),
            current: this.qs('[data-current]'),
            total: this.qs('[data-total]'),
            next: this.qs('[data-next]'),
            prev: this.qs('[data-prev]'),
            spriteContainer: this.qs('#sprite-container'),
            splits: opts.splits,
        });

        this.currentIndex = -1;
        this.isAnimating = false;
        this.totalSlides = this.ui.slides.length;
        this.currentIndex = [];
        this.nextSplits = [];
        this.slug = opts.slug;
        this.id = opts.id;
        this.sprites = [];
        this.title = opts.titles;
        this.tl = new gsap.TimelineMax({
            paused: true,
            onComplete: () => {
                this.currentIndex = this.nextIndex;
                this.isAnimating = false;
            },
        });


        // SETTING SPRITES
        this.params = opts.params;
        this.sprites = new Sprite({
            frames: 24,
            ticks: 1,
        });

        this.sprites.addSprites(this.params);
        this.bind();
    }

    bind() {
        this.scrolljack = new Scrolljack(this.selector);
        this.next = handleEvent('click', {
            onElement: this.ui.next,
            withCallback: () => {
                if (!this.isAnimating) {
                    this.scrollToSection(-1, true);
                }
            },
        });

        this.prev = handleEvent('click', {
            onElement: this.ui.prev,
            withCallback: () => {
                if (!this.isAnimating) {
                    this.scrollToSection(1, true);
                }
            },
        });

        emitter.on('scroll', (dir) => {
            if (!this.isAnimating) {
                this.scrollToSection(dir, true);
            }
        });
    }

    init() {
        this.currentIndex = 0;
        this.nextIndex = 0;
        this.$currentSection = this.ui.slides[this.nextIndex];
        this.$nextSection = this.ui.slides[this.nextIndex];

        gsap.set(this.ui.slides, {
            autoAlpha: 0,
        });

        // update numbers
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;
        this.ui.total.innerHTML = `0${this.totalSlides}`;

        classes.addClass(this.$nextSection, 'is-active');
        this.animateSections(1, true);
    }

    stop() {
        this.prev.destroy();
        this.next.destroy();

        this.scrolljack.stop();

        this.isAnimating = false;
    }

    scrollToSection(factor) {
        if (this.currentIndex + factor >= this.ui.slides.length || this.currentIndex + factor < 0 || this.isAnimating) {
            return;
        }

        this.nextIndex += factor;
        this.$currentSection = this.ui.slides[this.currentIndex];
        this.$nextSection = this.ui.slides[this.nextIndex];
        this.activeElements(factor);
    }

    activeElements(factor) {
        // change next index and slides
        this.$currentSection = this.ui.slides[this.currentIndex];
        this.$nextSection = this.ui.slides[this.nextIndex];

        // starting animation
        this.animateSections(factor, false);
    }

    animateSections(factor) {
        if (this.isAnimating) {
            return;
        }
        this.isAnimating = true;

        // update current number
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;

        this.setAnimations(factor).play();
    }

    setAnimations(factor) {
        this.ui.splits[this.currentIndex].forEach((el) => {
            const tween = gsap.staggerFromTo(el[0], 0.3, {
                autoAlpha: 1,
                y: 0,
            }, {
                autoAlpha: 0,
                y: -10,
                ease: gsap.Sine.easeOut,
            }, 0.005);

            this.currentSplits = new gsap.TimelineMax();
            this.currentSplits.add(tween);
        });

        this.ui.splits[this.nextIndex].forEach((el) => {
            const tween = gsap.staggerFromTo(el[0], 0.4, {
                autoAlpha: 0,
                y: 10,
            }, {
                autoAlpha: 1,
                y: 0,
                ease: gsap.Sine.easeOut,
            }, 0.03);

            this.nextSplits = new gsap.TimelineMax();
            this.nextSplits.add(tween);
        });

        // animate everything
        return this.tl
            .add(this.currentSplits)
            .set(this.$currentSection, {
                autoAlpha: 0,
                className: '-=is-active',
            })
            .add(() => {
                this.animateElements(factor);
            })
            .to(this.$nextSection, 0.2, {
                autoAlpha: 1,
                className: '+=is-active',
            })
            .add(this.nextSplits);
    }

    animateElements(factor) {
        if (!this.sprites) {
            return;
        }

        if (factor === 1) {
            if (this.nextIndex === 0) {
                this.sprites.animateIn(0);
            } else if (this.nextIndex === 2) {
                this.sprites.switchTo(0, 1);
            }
        } else {
            if (this.nextIndex === 1) {
                this.sprites.switchTo(1, 0);
            } else if (this.nextIndex === 2) {
                this.sprites.animateIn(1);
            }
        }
    }

    destroy() {
        this.stop();

        emitter.removeAllListeners('scroll');

        this.sprites.destroy();
        this.sprites = null;
    }
}

export default AboutSlideshow;
