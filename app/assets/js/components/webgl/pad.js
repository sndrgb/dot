import gsap from 'gsap';
import * as THREE from 'three';
import { getDevice, isMobile } from '../../base/utils';
import Visualizer from './visualizer';

let debug;

class Pad extends Visualizer {
    constructor() {
        super('pad');

        debug = false;
        this.scaleFactor = isMobile() ? 0.20 : 0.38;
        this.obj = this.scene.getObjectByName('pad');
    }

    load(params, done) {
        const jsonloader = new THREE.JSONLoader();
        const loader = new THREE.TextureLoader();

        loader.load(`/assets/images/${this.location}/diffuse.jpg`, (b) => {
            const material = new THREE.MeshPhongMaterial({
                map: b,
                side: THREE.DoubleSide,
                normalScale: new THREE.Vector2(0.8, 0.8),
            });

            jsonloader.load(`/assets/obj/${this.location}.js`, (geometry) => {
                const mesh = this.mesh = new THREE.Mesh(geometry, material);
                this.generateMesh(mesh, params);

                done();
            });
        });
    }

    generateObj(obj) {
        // set position
        let position;

        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            position = {
                x: -20,
                y: 80,
                z: 0,
            };
        } else {
            position = {
                x: 0,
                y: 50,
                z: 0,
            };
        }

        gsap.set(obj.position, position);

        gsap.set(obj.rotation, {
            x: 190 * (Math.PI / 180),
            y: 12 * (Math.PI / 180),
            z: Math.PI,
        });

        this.group.push(obj);
        this.scene.add(obj);
    }

    animateIn() {
        gsap.to(this.obj.position, 6, {
            y: 4,
            ease: Elastic.easeOut.config(1, 0.75),
            onComplete: () => {
                this.bindEvents();
            },
        });
    }

    animateOut() {
        if (this.mouseMoveEvent) {
            this.mouseMoveEvent.destroy();
        }

        gsap.to(this.obj.position, 1.5, {
            y: 80,
            ease: Elastic.easeIn.config(1, 1),
        });
    }

    render() {
        const delta = this.clock.getDelta();
        const timer = Date.now() * 0.0005;

        if (this.mesh) {
            const newMouseY = this.mouse.y / 4;
            const newMouseX = this.mouse.x / 18;

            gsap.to(this.mesh.rotation, 3, {
                x: (newMouseY - this.mesh.rotation.x) * 0.003,
                y: (newMouseX - this.mesh.rotation.y) * 0.003,
                easing: 'Expo.easeOut',
            });

            if (getDevice() === 'mobile' || getDevice() === 's-tablet') {
                this.mesh.position.x = Math.sin(timer);
            } else {
                this.mesh.position.x = Math.sin(timer) - 10;
            }

            this.mesh.position.y = Math.sin(timer) * 1.7;
            this.mesh.position.z = Math.cos(timer) * 2.7;
            this.mesh.geometry.computeBoundingBox();

            if (debug) {
                this.objHelper.update();
            }
        }

        this.renderer.render(this.scene, this.camera);
    }
}

export default Pad;
