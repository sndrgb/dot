import * as THREE from 'three';
import loop from 'raf-loop';
import { handleEvent, viewportSize } from '../../base/utils';
// import OrbitControls from 'three-orbitcontrols';

let debug;

class Visualizer {
    constructor(location) {
        const container = document.querySelector('#visualizer');
        this.clock = new THREE.Clock();
        this.mouseMoveEvent = null;
        this.group = [];
        this.location = location;
        this.windowHalfX = viewportSize().x / 2;
        this.windowHalfY = viewportSize().y / 2;
        this.mouse = {
            x: 0,
            y: 0,
        };

        this.renderer = new THREE.WebGLRenderer({ alpha: true });
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(viewportSize().x, viewportSize().y);
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

        container.appendChild(this.renderer.domElement);

        /* Main scene and camera */
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, viewportSize().x / viewportSize().y, 1, 1000);
        this.camera.position.z = 42;
        this.camera.position.x = 0;
        this.camera.position.y = 2;
        this.camera.lookAt(new THREE.Vector3(0, 0, 0));

        // const controls = new OrbitControls(this.camera, this.renderer.domElement);

        /* Various event listeners */
        window.addEventListener('resize', this.onResize.bind(this));

        this.setLights();
        this.addPlane();

        /* create and launch main loop */
        this.loop = loop(this.render.bind(this));
        this.loop.start();
    }

    init(params, done) {
        this.load(params, done);
    }

    generateMesh(mesh, params) {
        const vector = new THREE.Vector3();
        const matrix = new THREE.Matrix4();

        mesh.geometry.computeBoundingBox();
        const bbox = mesh.geometry.boundingBox;

        vector.subVectors(bbox.max, bbox.min);
        vector.multiplyScalar(0.5);
        vector.add(bbox.min);

        matrix.makeTranslation(-vector.x, -vector.y, -vector.z);
        mesh.geometry.applyMatrix(matrix);
        mesh.geometry.verticesNeedUpdate = true;
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        mesh.geometry.computeVertexNormals();
        mesh.scale.x = mesh.scale.y = mesh.scale.z = this.scaleFactor;
        mesh.matrixWorldNeedsUpdate = true;

        const obj = this.obj = new THREE.Object3D();
        obj.add(mesh);
        obj.name = params.id;

        this.generateObj(obj);
    }

    bindEvents() {
        this.mouseMoveEvent = handleEvent('mousemove', {
            onElement: window,
            withCallback: this.onMouseMove.bind(this),
        });
    }

    setLights() {
        const hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.8);
        hemiLight.color.setHSL(0, 0, 1);
        hemiLight.groundColor.setHSL(0, 0, 1);
        hemiLight.position.set(0, 500, 0);
        this.scene.add(hemiLight);

        const sunLight = this.sun = new THREE.DirectionalLight(0xffffff, 0.2);
        sunLight.color.setHSL(0, 0, 1);
        sunLight.position.set(0, 1.5, 1.5);
        sunLight.position.multiplyScalar(10);
        this.scene.add(sunLight);

        sunLight.castShadow = true;
        sunLight.shadow.mapSize.width = 2048;
        sunLight.shadow.mapSize.height = 2048;

        const d = 50;
        sunLight.shadow.camera.left = -d;
        sunLight.shadow.camera.right = d;
        sunLight.shadow.camera.top = d;
        sunLight.shadow.camera.bottom = -d;
        sunLight.shadow.camera.far = 3500;
        sunLight.shadow.bias = -0.0001;
    }

    addHelpers() {
        const axisHelper = new THREE.AxisHelper(50);
        this.scene.add(axisHelper);

        const helper = new THREE.GridHelper(200, 40, 0x0000ff, 0x808080);
        this.scene.add(helper);

        const directionV3 = new THREE.Vector3(1, 0, 1);
        const originV3 = new THREE.Vector3(0, 200, 0);
        const arrowHelper = new THREE.ArrowHelper(directionV3, originV3, 100, 0xff0000, 20, 10); // 100 is length, 20 and 10 are head length and width
        this.scene.add(arrowHelper);

        const cameraHelper = new THREE.CameraHelper(this.camera);
        cameraHelper.visible = true;
        this.scene.add(cameraHelper);
    }

    onResize() {
        if (this.camera) {
            this.camera.aspect = viewportSize().x / viewportSize().y;
            this.camera.updateProjectionMatrix();
        }

        this.renderer.setSize(viewportSize().x, viewportSize().y);
    }


    addPlane() {
        const planeGeom = new THREE.PlaneGeometry(500, 500, 32);
        // const planeMat = new THREE.MeshPhongMaterial();
        const planeMat = new THREE.ShadowMaterial();
        planeMat.side = THREE.DoubleSide;
        planeMat.opacity = 0.05;

        const plane = new THREE.Mesh(planeGeom, planeMat);
        plane.position.y = -15;
        plane.rotation.x = Math.PI / 2;
        plane.receiveShadow = true;
        this.scene.add(plane);
    }

    onMouseMove(e) {
        e.preventDefault();

        this.mouse = {
            x: e.clientX - this.windowHalfX,
            y: e.clientY - this.windowHalfY,
        };
    }

    destroy() {
        this.loop.stop();

        if (this.mouseMoveEvent) {
            this.mouseMoveEvent.destroy();
        }

        this.scene = null;
        this.camera = null;
    }
}

export default Visualizer;
