import gsap from 'gsap';
import * as THREE from 'three';
import merge from 'lodash/merge';
import { getDevice, isMobile } from '../../base/utils';
import Visualizer from './visualizer';

let debug;

class Sanity extends Visualizer {
    constructor() {
        super('sanity');

        debug = false;
        this.scaleFactor = isMobile() ? 0.18 : 0.30;
        this.obj = this.scene.getObjectByName('sanity');
    }

    load(params, done) {
        const jsonloader = new THREE.JSONLoader();

        const material = new THREE.MeshPhongMaterial({
            side: THREE.DoubleSide,
            normalScale: new THREE.Vector2(0.8, 0.8),
        });

        jsonloader.load(`/assets/obj/${this.location}.js`, (geometry) => {
            const mesh = this.mesh = new THREE.Mesh(geometry, material);
            this.generateMesh(mesh, params);
            done();
        });
    }

    generateObj(obj) {
        // set position
        let position;

        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            position = {
                x: -6,
                y: 80,
                z: 0,
            };
        } else {
            position = {
                x: 0,
                y: 50,
                z: 0,
            };
        }

        gsap.set(obj.position, position);
        gsap.set(obj.rotation, {
            x: 10 * (Math.PI / 180),
            y: -10 * (Math.PI / 180),
            z: -20 * (Math.PI / 180),
        });

        this.group.push(obj);
        this.scene.add(obj);
    }

    animateIn() {
        gsap.to(this.obj.position, 6, {
            y: 2,
            ease: gsap.Elastic.easeOut.config(1, 0.75),
            onComplete: () => {
                this.bindEvents();
            },
        });
    }

    animateOut() {
        if (this.mouseMoveEvent) {
            this.mouseMoveEvent.destroy();
        }

        gsap.to(this.obj.position, 1.5, {
            y: 80,
            ease: gsap.Elastic.easeIn.config(1, 1),
        });
    }

    render() {
        const delta = this.clock.getDelta();
        const timer = Date.now() * 0.0005;

        if (this.mesh) {
            const newMouseY = this.mouse.y / 4;
            const newMouseX = this.mouse.x / 6;

            gsap.to(this.mesh.rotation, 3, {
                y: (newMouseY - this.mesh.rotation.y) * 0.003,
                z: (newMouseX - this.mesh.rotation.z) * 0.003,
                easing: 'Expo.easeOut',
            });

            if (getDevice() === 'mobile' || getDevice() === 's-tablet') {
                this.mesh.position.x = Math.sin(timer);
            } else {
                this.mesh.position.x = Math.sin(timer) - 6;
            }

            this.mesh.position.y = Math.sin(timer) * 1.7;
            this.mesh.position.z = Math.cos(timer) * 2.7;
            this.mesh.geometry.computeBoundingBox();

            if (debug) {
                this.objHelper.update();
            }
        }

        this.renderer.render(this.scene, this.camera);
    }
}

export default Sanity;
