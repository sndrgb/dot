import classes from 'dom-helpers/class';
import gsap from 'gsap';
import emitter from '../base/emitter';
import { handleEvent } from '../base/utils';
import framework from '../pjax/framework';
import Component from './component';
import Scrolljack from './scrolljack';
import { productsConfig } from '../base/config';
import Tampon from '../components/webgl/tampon';
import Pad from '../components/webgl/pad';
import Liner from '../components/webgl/liner';
import Sanity from '../components/webgl/sanity';
import Sprite from '../components/sprite';

class Slideshow extends Component {
    constructor(selector, opts) {
        super(selector, opts);

        // una lista di nodi non è un array *****
        this.ui = Object.assign({}, this.ui, {
            slides: this.qsa('.c-slide'),
            current: this.qs('[data-current]'),
            total: this.qs('[data-total]'),
            next: this.qs('[data-next]'),
            prev: this.qs('[data-prev]'),
            scroll: this.qs('[data-scroll-down]'),
            nextProduct: this.qs('[data-next-product]'),
            bodies: opts.bodies,
        });

        this.currentIndex = -1;
        this.isAnimating = false;
        this.totalSlides = this.ui.slides.length;
        this.slug = opts.slug;
        this.id = opts.id;
        this.tl = new TimelineMax({
            onComplete: () => {
                this.currentIndex = this.nextIndex;
                this.isAnimating = false;
            },
        });

        this.params = productsConfig.find(obj => obj.id === this.id);

        // SETTING SPRITES
        this.sprite = new Sprite({
            frames: 24,
            ticks: 1,
        });

        this.sprite.addSprites([{
            id: opts.id,
            width: this.params.width,
            height: this.params.height,
        }]);


        // SETTING VISUALIZERS
        if (this.id === 'tampon') {
            this.visualizer = new Tampon();
        } else if (this.id === 'pad') {
            this.visualizer = new Pad();
        } else if (this.id === 'liner') {
            this.visualizer = new Liner();
        } else if (this.id === 'sanity') {
            this.visualizer = new Sanity();
        }
    }

    bind() {
        this.scrolljack = new Scrolljack('#slideshow');

        this.next = handleEvent('click', {
            onElement: this.ui.next,
            withCallback: () => {
                if (!this.isAnimating) {
                    this.scrollToSection(-1, true);
                }
            },
        });

        this.prev = handleEvent('click', {
            onElement: this.ui.prev,
            withCallback: () => {
                if (!this.isAnimating) {
                    this.scrollToSection(1, true);
                }
            },
        });

        emitter.on('scroll', (dir) => {
            if (!this.isAnimating) {
                this.scrollToSection(dir, true);
            }
        });
    }

    init() {
        this.bind();
        this.currentIndex = 0;
        this.nextIndex = 0;
        this.$currentSection = this.ui.slides[this.nextIndex];
        this.$nextSection = this.ui.slides[this.nextIndex];

        gsap.set(this.ui.scroll, { autoAlpha: 0 });
        gsap.set(this.ui.slides, { autoAlpha: 0 });

        // update numbers
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;
        this.ui.total.innerHTML = `0${this.totalSlides}`;

        // this.ui.nextProduct.innerHTML = this.params.next.replace('-', ' ');

        this.visualizer.init({}, () => {
            classes.addClass(this.$nextSection, 'is-active');
            this.animateSections(1, true);
        });
    }

    stop() {
        this.prev.destroy();
        this.next.destroy();

        this.scrolljack.stop();

        this.isAnimating = false;
    }

    scrollToSection(factor) {
        if (this.currentIndex + factor < 0 || this.isAnimating) {
            return;
        }

        if (this.currentIndex + factor >= this.ui.slides.length) {
            const lang = (localStorage.getItem('lang') === 'it') ? '' : 'en';
            framework.go(`${lang}/${this.params.next}`);
            this.stop();
            return;
        } else {
            this.nextIndex += factor;
            this.$currentSection = this.ui.slides[this.currentIndex];
            this.$nextSection = this.ui.slides[this.nextIndex];
            this.activeElements(factor);
        }
    }

    activeElements(factor) {
        // change next index and slides
        this.$currentSection = this.ui.slides[this.currentIndex];
        this.$nextSection = this.ui.slides[this.nextIndex];

        // starting animation
        this.animateSections(factor, false);
    }

    animateSections(factor) {
        if (this.isAnimating) {
            return;
        }
        this.isAnimating = true;

        // update current number
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;
        this.ui.current.innerHTML = `0${this.nextIndex + 1}`;


        // animate everything
        this.tl.staggerTo(this.ui.bodies[this.currentIndex], 0.3, {
            autoAlpha: 0,
            y: -10,
            ease: Sine.easeOut,
        }, 0.005).set(this.$currentSection, {
            autoAlpha: 0,
            className: '-=is-active',
        }).add(() => {
            this.animateElements(factor);
        }).to(this.$nextSection, 0.2, {
            autoAlpha: 1,
            className: '+=is-active',
        }).staggerFromTo(this.ui.bodies[this.nextIndex], 0.4, {
            autoAlpha: 0,
            y: 10,
        }, {
            autoAlpha: 1,
            y: 0,
            overwrite: 'all',
            ease: Sine.easeOut,
        }, 0.03);
    }

    animateElements(factor) {
        if (!this.visualizer || !this.sprite) {
            return;
        }

        if (factor === 1) {
            if (this.nextIndex === 0) {
                this.visualizer.animateIn();
            } else if (this.nextIndex === 2) {
                this.visualizer.animateOut();
                setTimeout(() => {
                    this.sprite.animateIn(0);
                }, 2000);

                gsap.to(this.ui.scroll, 0.5, {
                    autoAlpha: 1,
                });
            }
        } else {
            if (this.nextIndex === 1) {
                setTimeout(() => {
                    this.visualizer.animateIn();
                }, 2000);
                this.sprite.animateOut(0);
            }
        }
    }

    destroy() {
        this.stop();
        emitter.removeAllListeners('scroll');
        this.visualizer.destroy();
        this.sprite.destroy();
    }
}

export default Slideshow;
