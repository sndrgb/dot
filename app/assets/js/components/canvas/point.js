import _ from 'lodash';

class Point {

    constructor(x, y, params, context, originCenter) {

        this.ox = x;
        this.oy = y;
        this.x = x;
        this.y = y;
        this.maxX = 0;
        this.maxY = 0;
        this.targetX = 0;
        this.targetY = 0;
        this.velocityX = 0;
        this.velocityY = 0;
        this.maxDist = 0;
        this.params = params;
        this.context = context;
        this.originCenter = originCenter;

        _.bindAll(this, 'onMousemove');
        window.addEventListener('mousemove', this.onMousemove);
    }

    onMousemove(e) {
        this.mousePos = {
            x: e.pageX,
            y: e.pageY
        };
    }

    update() {

        if (this.mousePos) {

            const distance = this.distance(this.mousePos, this);
            const angle = this.getAngle(this.mousePos, this.originCenter);

            if (distance < this.maxDist) {

                const distRatio = 1 - distance / this.maxDist;
                this.targetX = this.ox + Math.cos(angle) * ((this.maxX - this.ox) * distRatio);

                this.targetY = this.oy + Math.sin(angle) * ((this.maxY - this.oy) * distRatio);

            } else {
                this.targetX = this.ox;
                this.targetY = this.oy;
            }

            this.velocityY += (this.targetY - this.y) * this.params.spring;
            this.y += this.velocityY *= 0.76;

            this.velocityX += (this.targetX - this.x) * this.params.spring;
            this.x += this.velocityX *= 0.76;
        }
    }

    distance(a, b) {
        return Math.sqrt(this.sqr(b.y - a.y) + this.sqr(b.x - a.x));
    }

    sqr(a) {
        return a * a;
    }

    getAngle(a, b) {
        return Math.atan2(a.y - b.y, a.x - b.x);
    }
}

export default Point