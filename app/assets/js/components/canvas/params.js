import { viewportSize, getDevice } from '../../base/utils';

// '#f9d0c8'

export function getParams() {
    const desktop = [
        {
            // obj 1
            color1: '#f9d0c8',
            color2: '#cfecd0',
            positions: [
                {
                    x: 0.19,
                    y: 0.57,
                }, {
                    x: 0.15,
                    y: 0.73,
                }, {
                    x: 0.14,
                    y: 0.75,
                }, {
                    x: 0.12,
                    y: 0.77,
                },
            ],
            rays: [4.2, 7.5, 7.5, 7.5],
        }, {
            // obj 2
            color1: '#f9d0c8',
            color2: '#b9deed',
            positions: [
                {
                    x: 0.51,
                    y: 0.35,
                }, {
                    x: 0.36,
                    y: 0.42,
                }, {
                    x: 0.29,
                    y: 0.44,
                }, {
                    x: 0.29,
                    y: 0.44,
                },
            ],
            rays: [6.5, 4.2, 6.8, 6.8],
        }, {
            // obj 3
            color1: '#f9d0c8',
            color2: '#f3f2c1',
            positions: [
                {
                    x: 0.69,
                    y: 0.56,
                }, {
                    x: 0.69,
                    y: 0.56,
                }, {
                    x: 0.63,
                    y: 0.56,
                }, {
                    x: 0.49,
                    y: 0.64,
                },
            ],
            rays: [8, 6.8, 4.2, 6.8],
        }, {
            // obj4
            color1: '#f9d0c8',
            color2: '#64b6bc',
            positions: [
                {
                    x: 0.88,
                    y: 0.77,
                }, {
                    x: 0.88,
                    y: 0.77,
                }, {
                    x: 0.88,
                    y: 0.77,
                }, {
                    x: 0.82,
                    y: 0.61,
                },
            ],
            rays: [6.5, 7, 7, 4.2],
        },
    ];

    const mobile = [
        {
            // obj 1
            color1: '#f9d0c8',
            color2: '#cfecd0',
            positions: [{
                x: 0.28,
                y: 0.26,
            }],
            rays: [7],
        }, {
            // obj 2
            color1: '#f9d0c8',
            color2: '#b9deed',
            positions: [{
                x: 0.74,
                y: 0.42,
            }],
            rays: [7],
        }, {
            // obj 3
            color1: '#f9d0c8',
            color2: '#f3f2c1',
            positions: [{
                x: 0.24,
                y: 0.57,
            }],
            rays: [7],
        }, {
            // obj4
            color1: '#f9d0c8',
            color2: '#64b6bc',
            positions: [{
                x: 0.74,
                y: 0.73,
            }],
            rays: [7],
        },
    ];

    const raw = (getDevice() === 'mobile' || getDevice() === 's-tablet') ? mobile : desktop;
    const data = raw.map(item => Object.assign(item, {
        positions: item.positions.map(position => ({
            x: position.x * viewportSize().x,
            y: position.y * viewportSize().y,
        })),
        rays: item.rays.map(ray => viewportSize().y / ray),
    }));

    return data;
}

export const others = [
    {
        positions: [{
            x: 0,
            y: viewportSize().y / 2,
        }],
        rays: [viewportSize().y / 2],
        color1: '#aa96c7',
        color2: '#f9d0c8',
    }, {
        positions: [{
            x: 0,
            y: viewportSize().y / 2,
        }],
        rays: [viewportSize().y / 2],
        color1: '#b3dbd3',
        color2: '#b9deed',
    }, {
        positions: [{
            x: 0,
            y: viewportSize().y / 2,
        }],
        rays: [viewportSize().y / 2],
        color1: '#aa96c7',
        color2: '#b9deed',
    },
];
