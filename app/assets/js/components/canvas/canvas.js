import gsap from 'gsap';
import delay from 'lodash/delay';
import ControlKit from 'controlkit';
import throttle from 'lodash/throttle';
import bindAll from 'lodash/bindAll';
import Bubble from './bubble';
import { getParams } from './params';
import { viewportSize, getDevice } from '../../base/utils';

class Canvas {
    constructor() {
        this.canvas = document.querySelector('.bubbles');
        this.context = this.canvas.getContext('2d');
        this.isFirst = true;
        this.circles = [];
        this.steps = 80;
        this.spring = 0.3;

        this.devMode = false;
        this.devPanel = null;

        this.params = getParams();
        this.init();

        bindAll(this, ['onResize']);
    }

    addEvent() {
        this.debounced = throttle(this.onResize, 50);
        window.addEventListener('resize', this.debounced);
    }

    init() {
        if (this.devMode) {
            this.controlKit = new ControlKit();
            this.devPanel = this.controlKit.addPanel();
        }

        this.context.canvas.width = viewportSize().x;
        this.context.canvas.height = viewportSize().y;

        this.params.forEach((param, i) => {
            // console.log(el);
            const id = (getDevice() === 'mobile' || getDevice() === 's-tablet') ? 0 : 1;

            // calculate gradient
            const yStart = param.positions[id].y + param.rays[id];
            const yEnd = param.positions[id].y - param.rays[id];

            const grd = this.context.createLinearGradient(0, yStart, 0, yEnd);
            grd.addColorStop(0, param.color2);
            grd.addColorStop(1, param.color1);

            delay(() => {
                const circle = new Bubble(this.context, param, grd, this.steps, id, this.devPanel);
                this.circles.push(circle);
            }, 300 * i);
        });

        gsap.ticker.addEventListener('tick', this.draw.bind(this));
    }

    onResize() {
        this.context.canvas.width = viewportSize().x;
        this.context.canvas.height = viewportSize().y;

        // updating parameters
        this.params = getParams();

        this.params.forEach((el, i) => {
            const params = (getDevice() === 'mobile' || getDevice() === 's-tablet') ? el[0] : el[1];

            if (this.circles) {
                this.circles[i].updateRadius(params.radius);
                this.circles[i].updateMoves({
                    x: params.x,
                    y: params.y,
                });
            }
        });
    }

    onMousemove(e) {
        this.mousePos = {
            x: e.pageX,
            y: e.pageY,
        };
    }

    updateBackground() {
        this.context.fillStyle = 'rgba(0,0,0,0)';
        this.context.beginPath();
        this.context.fillRect(0, 0, viewportSize().x, viewportSize().y);
        this.context.closePath();
    }

    switchTo(id) {
        this.params = getParams();

        this.circles.forEach((el, i) => {
            el.switchAnimation(this.params[i], id, 1.2);
        });
    }

    animateOut(done, id) {
        this.circles.forEach((el, i) => {
            if (id === i) {
                el.expand(done);
            } else {
                delay(el.animateOut.bind(el), 150 * i);
            }
        });
    }

    draw() {
        if (this.devMode) this.controlKit.update();

        this.context.clearRect(0, 0, viewportSize().x, viewportSize().y);
        this.updateBackground();

        this.circles.forEach((el) => {
            el.update();
        });

        if (this.isFirst) {
            this.isFirst = false;
            this.addEvent();
        }
    }

    destroy() {
        window.removeEventListener('resize', this.debounced);
        gsap.ticker.removeEventListener('tick', this.draw.bind(this));
    }
}

export default Canvas;
