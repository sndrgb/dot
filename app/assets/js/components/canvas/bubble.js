import gsap from 'gsap';
import { viewportSize } from '../../base/utils';
// import Point from './point'

class Bubble {
    constructor(context, params, grd, steps, id = 1, devPanel) {
        this.params = params;
        this.steps = steps;
        this.context = context;
        this.points = [];
        this.grd = grd;
        this.center = {
            x: params.positions[id].x,
            y: params.positions[id].y,
        };

        this.devPanel = devPanel;
        this.currentParams = {
            x: null,
            y: null,
            scale: this.params.rays[id],
        };

        // this.drawBubble();
        this.animateIn();
    }

    animateIn() {
        const moves = {
            y: window.innerHeight + this.center.y,
            x: this.center.x,
        };

        gsap.to(moves, 5, {
            y: this.center.y,
            x: this.center.x,
            ease: Elastic.easeOut.config(1, 0.5),
            onUpdate: this.updateMoves.bind(this),
            onUpdateParams: [moves],
            onComplete: () => {
                if (this.devPanel) this.addControl();
            },
        });
    }

    animateOut() {
        const y = -(this.center.y + window.innerHeight);

        gsap.to(this.currentParams, 6, {
            y,
            ease: Elastic.easeOut.config(1, 0.5),
        });
    }

    updateMoves(moves) {
        this.currentParams.x = moves.x;
        this.currentParams.y = moves.y;
    }

    updateRadius(radius) {
        this.currentParams.scale = radius;
    }

    switchAnimation(params, current, t = 0.8) {
        if (params !== undefined) {
            gsap.to(this.currentParams, t, {
                scale: params.rays[current],
                ease: Elastic.easeOut.config(1, 1),
            });
        }

        gsap.to(this.currentParams, t, {
            x: params.positions[current].x,
            y: params.positions[current].y,
            ease: Elastic.easeOut.config(1, 1),
        });
    }

    expand(done) {
        const point = this.currentParams;
        const windowPoints = [[0, 0], [window.innerWidth, 0], [0, window.innerHeight], [window.innerWidth, window.innerHeight]];

        const longest = windowPoints.reduce((prev, current) => {
            const hyp = Math.hypot(point.x - current[0], point.y - current[1]);

            if (Array.isArray(prev)) {
                return hyp;
            }

            return (hyp > prev) ? hyp : prev;
        });

        gsap.to(this.currentParams, 0.5, {
            scale: longest,
            delay: 0.2,
            ease: Sine.easeOut,
            onComplete: done,
        });
    }

    addControl() {
        const normalize = {
            x: (this.currentParams.x / viewportSize().x) * 100,
            y: (this.currentParams.y / viewportSize().y) * 100,
            scale: viewportSize().y / this.currentParams.scale,
        };

        const update = (id, index) => { this.currentParams[id] = (index * viewportSize()[id]) / 100; };
        const updateRadius = (index) => { this.currentParams.scale = (viewportSize().y / index); };

        this.devPanel
            .addGroup({ label: 'Ball' })
            .addNumberInput(normalize, 'x', { onChange: update.bind(this, 'x') })
            .addNumberInput(normalize, 'y', { onChange: update.bind(this, 'y') })
            .addNumberInput(normalize, 'scale', { onChange: updateRadius.bind(this) });
    }

    update() {
        this.context.fillStyle = this.grd;
        this.context.shadowBlur = 80;
        this.context.shadowColor = 'rgba(0, 0, 0, 0.17)';
        this.context.beginPath();

        this.context.arc(
            this.currentParams.x,
            this.currentParams.y,
            this.currentParams.scale,
            Math.PI * 2, false);

        this.context.closePath();
        this.context.fill();
    }
}

export default Bubble;

// drawBubble() {
//
//     const step = 2 * Math.PI / this.steps;
//
//     for (let i = 0; i < this.steps; i++) {
//
//         const theta = step * i;
//         const x =  this.center.x + this.params.radius * Math.cos(theta);
//         const y = this.center.y + this.params.radius * Math.sin(theta);
//
//         const point = new Point(x, y, this.params, this.context, this.center);
//
//         point.maxDist = this.params.radius;
//         point.maxX = Math.cos(Math.PI * 2) * 20;
//         point.maxY = Math.sin(Math.PI * 2) * 20;
//
//         this.points.push(point);
//     };
//
// }
