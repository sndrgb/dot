import gsap from 'gsap';
import 'gsap/src/uncompressed/plugins/ColorPropsPlugin';
import { getDevice } from '../../base/utils';
import { getParams, others } from './params';

class Background {
    constructor() {
        this.canvas = document.querySelector('#background');
        this.context = this.canvas.getContext('2d');
        this.isFirst = true;
        this.oldColors = null;
        this.window = {
            x: window.innerWidth,
            y: window.innerHeight,
        };

        this.addEvent();
        this.onResize();
    }

    setParams() {
        let params;

        // resolutions based on fullHD
        if (this.slug === 'contact' || this.slug === 'about' || this.slug === 'tss') {
            params = this.params = others[this.index];
        } else {
            params = this.params = getParams()[this.index];
        }

        return params;
    }

    addEvent() {
        window.addEventListener('resize', this.onResize.bind(this));
    }

    onResize() {
        this.context.canvas.width = window.innerWidth;
        this.context.canvas.height = window.innerHeight;

        this.window = {
            x: window.innerWidth,
            y: window.innerHeight,
        };
    }

    set(index, slug, from) {
        this.index = index;
        this.slug = slug;

        let pos;
        let ray;
        const params = this.setParams();
        const { positions, rays, color1, color2 } = params;

        const time = 3;

        if (getDevice() === 'mobile' || getDevice() === 's-tablet' || slug === 'about' || slug === 'contact' || slug === 'tss') {
            pos = positions[0];
            ray = rays[0];
        } else {
            pos = positions[1];
            ray = rays[this.index];
        }

        // set new position
        const newPosition = {
            start: pos.y + ray,
            end: pos.y - ray,
        };

        // set new colors
        const newColors = {
            color1,
            color2,
        };

        if (this.colors === undefined || this.colors === null || from === '/products' || from === '/') {
            this.colors = newColors;
        }

        if (this.position === undefined || this.position === null || from === '/products' || from === '/') {
            this.position = newPosition;
        }

        gsap.to(this.position, time, {
            start: newPosition.start,
            end: newPosition.end,
        });

        gsap.to(this.colors, time, {
            colorProps: {
                color1: newColors.color1,
                color2: newColors.color2,
            },
            onComplete: () => {
                this.oldColors = {
                    color1,
                    color2,
                };
            },
            onUpdate: () => {
                this.grd = this.context.createLinearGradient(0, this.position.start, 0, this.position.end);
                this.grd.addColorStop(0, this.colors.color2);
                this.grd.addColorStop(1, this.colors.color1);
            },
        });

        gsap.ticker.addEventListener('tick', this.draw.bind(this));
    }

    draw() {
        this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
        this.context.fillStyle = this.grd;
        this.context.beginPath();
        this.context.fillRect(0, 0, window.innerWidth, window.innerHeight);
        this.context.closePath();
    }
}

export const background = new Background();
