import gsap from 'gsap';
import bindAll from 'lodash/bindAll';
import { lerp, qsa } from '../base/utils';

class Parallax {
    constructor(el, id) {
        this.elements = document.querySelectorAll(`[data-parallax="${id}"]`);

        this.sx = 0;
        this.sy = 0;
        this.dx = this.sx;
        this.dy = this.sy;

        bindAll(this, ['onMove', 'render']);

        // const area = el.querySelector()
        this.area = el;
        const elRect = this.area.getBoundingClientRect();
        const elOffset = {
            top: elRect.top + document.body.scrollTop,
            left: elRect.left + document.body.scrollLeft,
        };

        this.fn = (e) => { this.onMove(e, elOffset); };
        this.area.addEventListener('mousemove', this.fn);
        window.requestAnimationFrame(this.render);
    }

    onMove(e, elOffset) {
        this.sx = (e.pageX - elOffset.left) - (this.area.clientWidth / 2);
        this.sy = (e.pageY - elOffset.top) - (this.area.clientHeight / 2);
    }

    render() {
        this.elements.forEach((el) => {
            const speed = el.getAttribute('data-speed') !== null ? parseFloat(el.getAttribute('data-speed')) : 1;

            const xParam = (speed * 10) * -1;
            const yParam = (speed * 10) * -1;

            this.dx = lerp(this.dx, this.sx, 0.1);
            this.dy = lerp(this.dy, this.sy, 0.1);

            gsap.to(el, 1, {
                x: this.dx / xParam.toFixed(2),
                y: this.dy / yParam.toFixed(2),
            });
        });

        window.requestAnimationFrame(this.render);
    }

    destroy() {
        this.area.removeEventListener('mousemove', this.fn);
    }
}

export default Parallax;
