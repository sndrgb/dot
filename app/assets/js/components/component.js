class Component {
    constructor(ui, options = {}) {
        this.ui = typeof ui === 'string' ? { container: document.querySelector(ui) } : { container: ui };
        this.settings = Object.assign({}, this.getDefaultOptions, options);
        this.events = {};
        this.state = new Map();
        this.listeners = new Map();
    }

    qs(selector) {
        return this.ui.container.querySelector(selector);
    }

    qsa(selector) {
        return Array.from(this.ui.container.querySelectorAll(selector));
    }

    queryClass(selector) {
        return Array.from(this.ui.container.getElementsByClassName(selector));
    }

    get getDefaultOptions() {
        return {};
    }

    setInitialState() {
        return this.state;
    }

    getState(key = undefined) {
        return (typeof key === 'undefined') ? this.state : this.state.get(key);
    }

    setState(key, newValue) {
        const oldValue = this.state.get(key);
        if (oldValue !== newValue) {
            this.state.set(key, newValue);
        }
    }

    getListeners() {
        return this.listeners;
    }

    setListener(key, context, value) {
        const scope = (typeof context === 'string') ? document.querySelectorAll(context) : context;
        const addListener = (key, scope, value) => {
            if (scope.hasOwnProperty(length)) {
                for (let i = scope.length; i--;) {
                    scope[i].addEventListener(key, value, false);
                }
            } else {
                scope.addEventListener(key, value, false);
            }
        };

        if (this.listeners.has(scope)) {
            let oldMap = this.listeners.get(scope);
            if (oldMap.has(key)) {
                let oldFunctions = oldMap.get(key);
                if (oldFunctions.indexOf(value) === -1) {
                    oldFunctions.push(value);
                    addListener(key, scope, value);
                }
            } else {
                const newItem = new Map();
                newItem.set(key, [value]);
                this.listeners.set(scope, newItem);
                addListener(key, scope, value);
            }
        } else {
            const newItem = new Map();
            newItem.set(key, [value]);
            this.listeners.set(scope, newItem);
            addListener(key, scope, value);
        }

        return this.listeners;
    }
}

export default Component;