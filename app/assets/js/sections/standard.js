import classes from 'dom-helpers/class';
import { qsa } from '../base/utils';
import config from '../pjax/config';
import utils from '../pjax/utils';

class Standard {
    constructor() {
        this.view = config.$view;
        this.page = null;
        this.a = null;
        this.ui = {};
    }


    init(req, done, options) {
        const opts = options || { cache: true, sub: false };
        const view = this.view;
        const ready = this.dataAdded.bind(this, done);

        const page = this.page = req.previous === undefined ? config.$view.querySelector('.page') : utils.loadPage(req, view, ready, opts);

        if (req.previous === undefined) {
            classes.removeClass(page, 'is-hidden');

            setTimeout(() => {
                ready();
            }, 200);
        }
    }

    dataAdded() {
        this.a = qsa('a', this.page);
        utils.addRoutingEL(this.a);
    }

    destroy() {
        if (this.a) {
            utils.removeRoutingEL(this.a);
        }
        this.a = null;
    }
}

export default Standard;
