import gsap from 'gsap';
import bindAll from 'lodash/bindAll';
import debounce from 'lodash/debounce';
import classes from 'dom-helpers/class';
import config from '../pjax/config';
import { getDevice, qsa, isMobile } from '../base/utils';
import Standard from './standard';
import { aboutConfig } from '../base/config';
import { background } from '../components/canvas/background';
import AboutSlideshow from '../components/about-slideshow';
import 'gsap/src/uncompressed/utils/SplitText';

class About extends Standard {
    constructor(opt) {
        super(opt);
        this.slug = 'about';
        this.oldSize = isMobile();

        bindAll(this, ['onResize']);
    }

    init(req, done) {
        super.init(req, done);

        this.debounced = debounce(this.onResize, 100);
        window.addEventListener('resize', this.debounced);

        this.paramId = req.params.id;
        this.splits = [];
        this.sprites = [];
    }

    onResize() {
        if (isMobile() !== this.oldSize) {
            window.location.reload(true);
        }

        this.oldSize = isMobile();
    }

    dataAdded(done) {
        super.dataAdded();

        background.set(0, this.slug);

        done();
    }

    animateIn(req, done) {
        classes.addClass(config.$body, `is-${this.slug}`);

        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            const slides = qsa('.c-slide', this.page);
            const title = qsa('[data-anim="split"]', this.page);
            this.title = new SplitText(title, { type: 'chars' }).chars;
            gsap.set(this.title, { autoAlpha: 0, y: 20 });

            slides.forEach((el, i) => {
                const bodies = qsa('[data-anim="text"]', el);
                this.splits[i] = [];

                bodies.forEach((body, n) => {
                    const splitBody = new SplitText(body, { type: 'words' }).words;
                    this.splits[i][n] = [];
                    this.splits[i][n].push(splitBody);
                    gsap.set(splitBody, { autoAlpha: 0, y: 20 });
                });
            });


            this.slideshow = new AboutSlideshow('#about-slideshow', {
                id: this.paramId,
                slug: this.slug,
                splits: this.splits,
                title: this.title,
                params: aboutConfig,
            });

            this.slideshow.init();
        }

        gsap.to(this.page, 1, {
            autoAlpha: 1,
            ease: Expo.easeInOut,
        });

        gsap.staggerFromTo(this.title, 0.3, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 1,
            overwrite: 'all',
            ease: Sine.easeOut,
        }, 0.02);

        gsap.fromTo('.c-slide-nav', 0.6, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 1,
            ease: Sine.easeOut,
            onComplete: done,
        });
    }

    animateOut(req, done) {
        classes.removeClass(config.$body, `is-${this.slug}`);

        gsap.to(this.page, 0.7, {
            autoAlpha: 0,
            ease: 'Expo.easeInOut',
            onComplete: done,
        });
    }

    destroy(req, done) {
        super.destroy();

        window.removeEventListener('resize', this.debounced);

        this.splits = [];
        this.title = [];

        if (this.slideshow) {
            this.slideshow.destroy();
        }

        this.page.parentNode.removeChild(this.page);
        done();
    }
}

module.exports = About;
