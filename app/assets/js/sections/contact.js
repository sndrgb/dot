import gsap from 'gsap';
import approve from 'approvejs';
import superagent from 'superagent';
import debounce from 'lodash/debounce';
import 'whatwg-fetch';
import classes from 'dom-helpers/class';
import config from '../pjax/config';
import Standard from './standard';
import { qs, qsa, handleEvent } from '../base/utils';
import { background } from '../components/canvas/background';

class Contact extends Standard {

    constructor(opt) {
        super(opt);
        this.slug = 'contact';
        this.ui = null;

        this.splits = [];
    }

    init(req, done) {
        super.init(req, done);
        background.set(1, this.slug);
    }

    dataAdded(done) {
        super.dataAdded();

        this.ui = {
            bodies: qsa('[data-anim="text"]', this.page),
            titles: qsa('[data-anim="split"]', this.page),
            container: qs('.c-form', this.page),
        };

        this.splitTitle = new SplitText(this.ui.titles, { type: 'chars' }).chars;
        gsap.set(this.splitTitle, { autoAlpha: 0, y: 20 });

        this.ui.bodies.forEach((el) => {
            const splitBody = new SplitText(el, { type: 'words' }).words;
            gsap.set(splitBody, { autoAlpha: 0, y: 20 });
            this.splits.push(splitBody);
        });


        gsap.set('.c-form__row, [data-message]', {
            autoAlpha: 0,
            yPercent: 50,
        });

        gsap.staggerTo('.c-form__row', 0.6, {
            autoAlpha: 1,
            yPercent: 0,
        }, 0.1);

        // const debounced = debounce(this.callAjax.bind(this), 200);
        
        // setting up contact form
        // this.sendMail = handleEvent('submit', {
        //     onElement: document.querySelector('#form-contact'),
        //     withCallback: (e) => {
        //         console.log('click');
        //         e.preventDefault();
        //         debounced();
        //     },
        // }, this);

        done();
    }

    animateIn(req, done) {
        classes.addClass(config.$body, `is-${this.slug}`);

        gsap.to(this.page, 1, {
            autoAlpha: 1,
            ease: Expo.easeInOut,
            onComplete: done,
        });

        gsap.staggerTo(this.splitTitle, 0.3, {
            autoAlpha: 1,
            y: 0,
            ease: Sine.easeOut,
        }, 0.02);

        this.splits.forEach((el) => {
            gsap.staggerTo(el, 0.4, {
                autoAlpha: 1,
                y: 0,
                ease: Sine.easeOut,
            }, 0.04);
        });
    }

    animateOut(req, done) {
        classes.removeClass(config.$body, `is-${this.slug}`);

        gsap.to(this.page, 0.7, {
            autoAlpha: 0,
            ease: Expo.easeInOut,
            onComplete: done,
        });
    }

    destroy(req, done) {
        super.destroy();

        this.ui = null;

        this.page.parentNode.removeChild(this.page);

        done();
    }

    callAjax() {
        const headers = {};
        const rule = {
            required: true,
        };

        this.data = [{
            name: 'fromName',
            container: this.ui.container.querySelector('#fromName'),
            value: this.ui.container.querySelector('#fromName').value,
            test: rule,
        },
        // {
        //     name: 'email',
        //     container: this.ui.container.querySelector('#email'),
        //     value: this.ui.container.querySelector('#email').value,
        //     test: { required: true, email: true },
        // }, {
        //     name: 'phone',
        //     container: this.ui.container.querySelector('#telephone'),
        //     value: this.ui.container.querySelector('#telephone').value,
        //     test: null,
        // },
        {
            name: 'message',
            container: this.ui.container.querySelector('#message'),
            value: this.ui.container.querySelector('#message').value,
            test: rule,
        },
        // {
        //     name: 'privacy',
        //     container: this.ui.container.querySelector('#privacy-check'),
        //     value: this.ui.container.querySelector('#privacy-check').checked,
        //     test: rule,
        // }
        ];

        this.data.forEach((obj) => {
            if (obj.test !== null) {
                const result = approve.value(obj.value, obj.test);

                if (!result.approved) {
                    gsap.set(obj.container, {
                        className: '+=is-wrong',
                    });
                } else {
                    gsap.set(obj.container, {
                        className: '-=is-wrong',
                    });

                    Object.assign(headers, {
                        [obj.name]: obj.value,
                    });
                }
            } else {
                Object.assign(headers, {
                    [obj.name]: obj.value,
                });
            }

            if (Object.keys(headers).length === this.data.length) {
                headers[window.csrfTokenName] = window.csrfTokenValue;

                console.log(headers);

                superagent.post('/').set('Content-Type', 'application/x-www-form-urlencoded').send(headers).end((err, res) => {
                    if (res.ok) {
                        gsap.staggerTo('.c-form__row', 0.6, {
                            autoAlpha: 0,
                            yPercent: 50,
                            onComplete: () => {
                                gsap.to('[data-message]', 0.5, {
                                    autoAlpha: 1,
                                    yPercent: 0,
                                });
                            },
                        }, 0.1);
                    }
                });
            }
        });
    }
}

module.exports = Contact;

