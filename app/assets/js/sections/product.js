import { TweenMax, Sine } from 'gsap';
import debounce from 'lodash/debounce';
import bindAll from 'lodash/bindAll';
import classes from 'dom-helpers/class';
import config from '../pjax/config';
import { getDevice, qsa, isMobile } from '../base/utils';
import 'gsap/src/uncompressed/utils/SplitText';
import Standard from './standard';
import { background } from '../components/canvas/background';
import Slideshow from '../components/slideshow';
import Tampon from '../components/webgl/tampon';
import Pad from '../components/webgl/pad';
import Liner from '../components/webgl/liner';
import Sanity from '../components/webgl/sanity';

class Product extends Standard {
    constructor(opt) {
        super(opt);
        this.slug = 'product';
        this.oldSize = isMobile();

        bindAll(this, ['onResize']);
    }

    init(req, done) {
        super.init(req, done);

        this.debounced = debounce(this.onResize, 100);
        window.addEventListener('resize', this.debounced);

        this.paramId = req.params.id;
        this.from = req.previous ? req.previous.route : '';
        this.splits = {
            titles: [],
            bodies: [],
        };
    }

    onResize() {
        if (isMobile() !== this.oldSize) {
            window.location.reload(true);
        }

        this.oldSize = isMobile();
    }

    dataAdded(done) {
        super.dataAdded();

        if (this.paramId === 'applicator-tampons') {
            background.set(0, this.slug, this.from);
            this.paramId = 'tampon';
        } else if (this.paramId === 'hygenic-pad') {
            background.set(1, this.slug, this.from);
            this.paramId = 'pad';
        } else if (this.paramId === 'panty-liner') {
            background.set(2, this.slug, this.from);
            this.paramId = 'liner';
        } else if (this.paramId === 'night-pads') {
            background.set(3, this.slug, this.from);
            this.paramId = 'sanity';
        }

        this.ui = {
            titles: qsa('[data-anim="split"]', this.page),
            bodies: qsa('[data-anim="text"]', this.page),
        };

        done();
    }

    animateIn(req, done) {
        classes.addClass(config.$body, `is-${this.slug}`);
        classes.addClass(config.$body, `is-${this.paramId}`);


        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            this.ui.titles.forEach((el) => {
                const splitTitle = new SplitText(el, { type: 'chars' }).chars;
                this.splits.titles.push(splitTitle);
                TweenMax.set(splitTitle, { autoAlpha: 0, y: 20 });
            });

            this.ui.bodies.forEach((el) => {
                const splitBody = new SplitText(el, { type: 'words' }).words;
                this.splits.bodies.push(splitBody);
                TweenMax.set(splitBody, { autoAlpha: 0, y: 20 });
            });

            this.slideshow = new Slideshow('#slideshow', {
                id: this.paramId,
                slug: this.slug,
                bodies: this.splits.bodies,
            });

            this.slideshow.init();
        } else {
            if (this.paramId === 'tampon') {
                this.visualizer = new Tampon();
            } else if (this.paramId === 'pad') {
                this.visualizer = new Pad();
            } else if (this.paramId === 'liner') {
                this.visualizer = new Liner();
            } else if (this.paramId === 'sanity') {
                this.visualizer = new Sanity();
            }

            this.visualizer.init({}, () => {
                this.visualizer.animateIn();
            });
        }

        TweenMax.set(this.page, { autoAlpha: 1 });

        TweenMax.staggerFromTo(this.splits.titles[0], 0.3, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 1,
            overwrite: 'all',
            ease: Sine.easeOut,
            onComplete: () => {
                done();
            },
        }, 0.02);

        TweenMax.fromTo('.c-slide-nav', 0.6, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 1,
            ease: Sine.easeOut,
        });

        TweenMax.fromTo('.c-table', 0.6, {
            autoAlpha: 0,
            y: 20,
        }, {
            autoAlpha: 1,
            y: 0,
            delay: 1,
            ease: Sine.easeOut,
        });
    }

    animateOut(req, done) {
        classes.removeClass(config.$body, `is-${this.slug}`);
        classes.removeClass(config.$body, `is-${this.paramId}`);

        TweenMax.to(this.page, 0.7, {
            autoAlpha: 0,
            ease: 'Expo.easeInOut',
            onComplete: done,
        });
    }

    destroy(req, done) {
        super.destroy();

        window.removeEventListener('resize', this.debounced);

        this.splits = {
            titles: [],
            bodies: [],
        };

        if (this.slideshow) {
            this.slideshow.destroy();
        }

        this.page.parentNode.removeChild(this.page);
        done();
    }
}

module.exports = Product;
