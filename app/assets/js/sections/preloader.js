import classes from 'dom-helpers/class';
import gsap from 'gsap';
import 'whatwg-fetch';
import config from '../pjax/config';
import { getDevice } from '../base/utils';
// import Request from '../components/request';
import Navigation from '../components/navigation';

class Preloader {
    constructor(onComplete) {
        this.preloaded = onComplete;
        this.view = config.$view;
        this.el = document.getElementById('loader');
        this.wrapper = this.el.querySelector('.c-loader__wrapper');
        this.loader = this.el.querySelector('[data-loader]');
        this.modals = [];
    }

    init(req, done) {
        fetch(`${config.BASE}data/data.json`).then(res => res.text()).then((object) => {
            window._data = object;

            // const request = new Request('[data-overlay="request"]');
            classes.addClass(config.$body, 'is-loading');

            if (getDevice() === 'mobile' || getDevice() === 's-tablet') {
                const navigation = new Navigation('[data-overlay="menu"]');
            }

            done();
            //     });
        });
    }

    animateIn(req, done) {
        const counter = { var: 0 };
        const tl = new TimelineMax({
            paused: true
        });

        tl.set('.c-loader__text', {
            autoAlpha: 1,
        })
        .fromTo(this.wrapper, 3, {
            yPercent: 80,
            skewY: -50,
        }, {
            yPercent: 0,
            skewY: 0,
            ease: Expo.easeOut,
        })
        .fromTo('.c-loader__text', 3, {
            yPercent: 300,
        }, {
            yPercent: 0,
            ease: Expo.easeOut,
        }, '-=3')
        .to(this.wrapper, 3, {
            scale: 0.8,
            ease: Expo.easeOut,
        }, '-=1.2')
        .to(this.wrapper, 2, {
            yPercent: -80,
            skewY: 50,
            ease: Expo.easeIn,
            onComplete: () => {
                this.preloaded();
                done();
            },
        }, '-=2')
        .to('.c-loader__text', 2, {
            yPercent: 0,
        }, {
            yPercent: -300,
            ease: Expo.easeIn,
        }, '-=2')
        .to('.c-loader', 0.5, {
            autoAlpha: 0,
        }, '-=0.5')
        .fromTo('#js-page', 0.8, {
            autoAlpha: 0,
        }, {
            autoAlpha: 1,
            clearProps: 'all',
        }).fromTo('#background', 0.8, {
            autoAlpha: 0,
        }, {
            autoAlpha: 1,
            clearProps: 'all',
        }, '-=0.8');

        gsap.to(counter, 3, {
            var: 50,
            onUpdate: () => {
                this.loader.innerHTML = Math.ceil(counter.var) * 2;
            },
        });

        tl.play();
    }

    animateOut(req, done) {
        gsap.to(this.loader, 1, {
            autoAlpha: 0,
            onComplete: done,
        });
    }

    destroy(req, done) {
        classes.addClass(config.$body, 'is-loaded');
        classes.removeClass(config.$body, 'is-loading');

        document.body.removeChild(this.el);
        done();
    }
}

module.exports = Preloader;
