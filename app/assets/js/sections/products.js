import gsap from 'gsap';
import bindAll from 'lodash/bindAll';
import debounce from 'lodash/debounce';
import delay from 'lodash/delay';
import classes from 'dom-helpers/class';
import 'gsap/src/uncompressed/plugins/DrawSVGPlugin';
import 'gsap/src/uncompressed/utils/SplitText';
import config from '../pjax/config';
import { getDevice, qsa, isMobile } from '../base/utils';
import Canvas from '../components/canvas/canvas';
import Standard from './standard';
import Parallax from '../components/parallax';


class Products extends Standard {

    constructor(opt) {
        super(opt);

        this.slug = 'products';
        this.tls = [];
        this.listeners = [];
        this.oldId = 0;
        this.oldSize = isMobile();

        bindAll(this, ['setProducts', 'onResize']);
    }

    init(req, done) {
        super.init(req, done);

        this.debounced = debounce(this.onResize, 100);
        window.addEventListener('resize', this.debounced);
    }

    onResize() {
        if (isMobile() !== this.oldSize) {
            window.location.reload(true);
        }

        this.oldSize = isMobile();
    }

    bindEvents() {
        if (this.ui && this.ui.products) {
            this.ui.products.forEach((el) => {
                const id = parseInt(el.getAttribute('data-product'), 0);

                const mouseInHandler = () => this.mouseEnter(el, id);
                const mouseOutHandler = e => this.mouseLeave(e, el, id);

                el.addEventListener('mouseenter', mouseInHandler, false);
                el.addEventListener('mouseleave', mouseOutHandler, false);

                this.listeners.push([mouseInHandler, mouseOutHandler]);
            });
        }
    }

    unbindEvents() {
        this.ui.products.forEach((el, i) => {
            const listener = this.listeners[i];

            if (listener !== undefined) {
                el.removeEventListener('mouseenter', listener[0], false);
                el.removeEventListener('mouseleave', listener[1], false);
            }
        });
    }

    dataAdded(done) {
        super.dataAdded();

        this.ui = {
            products: qsa('[data-product]', document),
            borders: qsa('[data-border]', document),
            rects: qsa('rect', document),
        };

        this.setProducts();
        done();
    }

    setProducts() {
        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            gsap.set(this.ui.rects, {
                drawSVG: '0%',
            });

            this.ui.products.forEach((el, i) => {
                const topTexts = qsa('[data-text="top"]', this.ui.products[i]);
                const bottomTexts = qsa('[data-text="bottom"]', this.ui.products[i]);

                const top = new SplitText(topTexts, { type: 'words, chars' }).chars;
                const bottom = new SplitText(bottomTexts, { type: 'words, chars' }).chars;

                gsap.set(top, { autoAlpha: 0 });
                gsap.set(bottom, { autoAlpha: 0 });

                const params = {
                    top,
                    bottom,
                };

                this.tls.push(params);
            });
        }
    }

    animateIn(req, done) {
        const firstEl = this.ui.products[1];
        classes.addClass(config.$body, `is-${this.slug}`);
        classes.addClass(firstEl, 'is-active');

        this.canvas = new Canvas();

        gsap.to(this.page, 1, {
            autoAlpha: 1,
            ease: 'Expo.easeInOut',
            onComplete: done,
        });

        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
            delay(() => {
                this.mouseEnter(firstEl, 1);
                this.oldId = 1;
            }, 4000);

            delay(() => {
                this.bindEvents();
            }, 4200);
        }
    }

    animateOut(req, done) {
        if (getDevice() !== 'mobile' && getDevice() !== 's-tablet') {
           this.unbindEvents();
        }

        gsap.to('[data-text]', 0.2, {
            autoAlpha: 0,
        });

        classes.removeClass(config.$body, `is-${this.slug}`);

        if (req.params.id === 'applicator-tampons') {
            this.canvas.animateOut(done, 0);
        } else if (req.params.id === 'hygenic-pad') {
            this.canvas.animateOut(done, 1);
        } else if (req.params.id === 'panty-liner') {
            this.canvas.animateOut(done, 2);
        } else if (req.params.id === 'night-pads') {
            this.canvas.animateOut(done, 3);
        } else {
            gsap.to(this.page, 0.7, {
                autoAlpha: 0,
                ease: 'Expo.easeInOut',
                onComplete: done,
            });
        }
    }

    mouseEnter(el, id) {
        if (el.getAttribute('is-hover') === true) {
            return;
        }

        if (this.outOfScreen) {
            this.outOfScreen = false;
        }

        if (id === null || id === undefined) {
            id = 1;
        } else {
            this.canvas.switchTo(id);
        }

        if (id !== this.oldId) {
            this.mouseOut(this.oldId);
            classes.addClass(this.ui.products[id], 'is-active');
            classes.addClass(this.ui.borders[id], 'is-active');

            delay(() => {
                this.parallax = new Parallax(el, id);
            }, 1000);

            gsap.to(this.ui.rects[id], 0.6, {
                drawSVG: '100%',
                ease: 'Sine.easeOut',
                delay: 0.5,
            });

            gsap.staggerFromTo(this.tls[id].top, 0.25, {
                yPercent: -20,
                autoAlpha: 0,
            }, {
                delay: 0.55,
                yPercent: 0,
                autoAlpha: 1,
                ease: 'Sine.easeOut',
                onComplete: () => {
                    el.setAttribute('is-hover', true);
                },
            }, 0.02);

            gsap.staggerFromTo(this.tls[id].bottom, 0.25, {
                yPercent: 20,
                autoAlpha: 0,
            }, {
                delay: 0.55,
                yPercent: 0,
                autoAlpha: 1,
                ease: 'Sine.easeOut',
            }, 0.02);
        }
    }

    mouseLeave(e, el, id) {
        const from = e.relatedTarget || e.toElement;

        if (!from || from.nodeName === 'HTML') {
            this.outOfScreen = true;
            if (id !== this.oldId) {
                this.mouseOut(this.oldId);
            }

            this.oldId = id;
            return;
        }

        this.oldId = id;
    }

    mouseOut(id) {
        const el = this.ui.products[id];
        classes.removeClass(this.ui.products[id], 'is-active');
        classes.removeClass(this.ui.borders[id], 'is-active');

        if (this.parallax) {
            this.parallax.destroy();
        }

        el.setAttribute('is-hover', false);

        gsap.to(this.ui.rects[id], 0.3, {
            drawSVG: '0%',
            ease: 'Sine.easeOut',
            overwrite: 'all',
        });

        gsap.to(this.tls[id].top, 0.2, {
            autoAlpha: 0,
            ease: 'Sine.easeOut',
            overwrite: 'all',
        });

        gsap.to(this.tls[id].bottom, 0.2, {
            autoAlpha: 0,
            ease: 'Sine.easeOut',
            overwrite: 'all',
        });
    }

    destroy(req, done) {
        super.destroy();

        window.removeEventListener('resize', this.debounced);

        if (this.canvas !== undefined) {
            this.canvas.destroy();
        }

        this.tls = [];
        this.listeners = [];
        this.oldId = 0;
        this.ui = null;
        this.page.parentNode.removeChild(this.page);

        done();
    }
}

module.exports = Products;
