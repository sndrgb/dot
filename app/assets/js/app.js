import framework from './pjax/framework';
import utils from './pjax/utils';

const links = Array.from(document.querySelectorAll('a'));
links.forEach((el) => {
    el.addEventListener('click', utils.handleRoute);
});

const lang = document.documentElement.lang;
localStorage.setItem('lang', lang);

// const currentTitle = document.title;
// window.onblur = () => { document.title = '• DOT - Assorbenti esterni e tamponi.'; };
// window.onfocus = () => { document.title = currentTitle; };

framework.init();
