/**
 * Gulp build tasks
 **/

var fs = require('fs'),
    path = require('path'),
    gulp = require('gulp'),
    runSequence = require('run-sequence'),
    _ = require('lodash'),
    $ = require('gulp-load-plugins')(),
    argv = require('yargs').argv || {},
    taskPath = path.join(process.cwd(), 'build', 'gulp-tasks'),
    optionsPath = path.join(process.cwd(), 'build'),
    options = require(optionsPath + '/options');


_.forOwn({
    production: false,
    command: null,
    remotehost: null //be explicit!
}, function (value, key) {
    options[key] = argv.hasOwnProperty(key) ? argv[key] : value;
});

// force production env
if (options.production) {
    process.env.NODE_ENV = 'production';
}

// unique build identifier for browsersync
options.buildHash = 'buildhash' + (Date.now());

options.assetsPath = function (type, match) {
    var parts = type.split('.'),
        paths = options.paths,
        folderPath;
    if (parts.length > 1) {
        folderPath = path.join(paths[parts[0]].assets, paths[parts[1]]);
    } else {
        folderPath = path.normalize(paths[parts[0]].assets);
    }
    if (match) {
        folderPath = path.join(folderPath, match);
    }
    return folderPath;
};

// require all tasks
fs.readdirSync(taskPath).filter((taskFile) => {
    return path.extname(taskFile) === '.js';
}).forEach((taskFile) => {
    require(`${taskPath}/${taskFile}`)(gulp, $, options);
});

gulp.task('default', ['clean'], (done) => {
    const tasks = ['images', ['fonts', 'media', 'styles', 'scripts'], 'modernizr', 'views'];

    if (options.styleguideDriven) {
        tasks.push('styleguide');
    }

    tasks.push(done);
    runSequence.apply(null, tasks);
});

gulp.task('dev', ['default', 'server']);
