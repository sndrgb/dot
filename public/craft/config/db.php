<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
    '*' => array(
        'tablePrefix' => 'craft',
    ),
    
	'dev' => array(
        'server' => 'localhost',
        'user' => 'root',
        'password' => 'root',
        'database' => 'dot',
    ),

    'prod' => array(
        'server' => 'localhost',
        'user' => 'z422clog_root',
        'password' => 'Ad10310568',
        'database' => 'z422clog_dot',
    ),
);