const srcRoot = 'app';
const distRoot = 'public/public';

module.exports = {

    hosts: {

        staging: {
            host: 'staging.host',
            username: 'username',
            password: 'fancypassword',
            path: '/home/httpd/virtualhost',
        },

        production: {
            host: 'production.host',
            username: 'username',
            password: 'fancypassword',
            path: '/home/httpd/virtualhost',
        },

        // remote host of developer box for mobile debug with weinre
        devbox: {
            ports: {
                weinre: 3200,
                connect: 3100, // optional port for standalone static server
            },
        },
    },

    paths: {

        src: {
            root: srcRoot,
            assets: `${srcRoot}/assets`,
            fixtures: `${srcRoot}/handlers`,
            views: `${srcRoot}/views`,
        },

        dist: {
            root: distRoot, // where static files are to be saved
            assets: `${distRoot}/assets`,
            views: 'public', // when working on with CMS, views may be stored in a different folder
        },

        js: 'js',
        sass: 'scss',
        css: 'css',
        images: 'images',
        fonts: 'fonts',
        audio: 'audio',
        video: 'video',
        vendors: 'vendors',
        tmp: '.tmp',
    },

    deployStrategy: 'ftp', // `rsync` or `ftp`
    livereload: true, // set to `true` to enable livereload
    styleguideDriven: false, // will rebuild the styleguide whenever stylesheets change
    buildOnly: false, // set to `true` when paired with Phing
    viewmatch: '*.html',  // for php projects use: '*.{html,php,phtml}'
};
