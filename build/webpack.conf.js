module.exports = function (options) {

    var path = require('path'),
        _ = require('lodash'),
        webpack = require('webpack');

    var srcPath = path.join(process.cwd(), options.assetsPath('src.js')),
        destPath = path.join(options.assetsPath('dist.js')),
        plugins = [];


    // set webpack plugins
    plugins.push(
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
            'PRODUCTION': options.production,
            'process.env': {
                'NODE_ENV': JSON.stringify(options.production ? 'production' : 'development')
            }
        })
        // new webpack.ProvidePlugin({
        //     'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        // })
    );

    if (options.production) {
        plugins.push(
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.UglifyJsPlugin({
                sourceMap: true,
                compressor: {
                    screw_ie8: true,
                    warnings: false,
                },
            })
        );
    }

    return {
        context: srcPath,
        entry: [],
        output: {
            path: path.join(process.cwd(), destPath),
            publicPath: destPath.replace(options.paths.dist.root, '').replace(/\\/g, '/') + '/',
            filename: 'build.js',
        },
        watch: !!options.isWatching,
        devtool: (options.production ? 'source-map' : 'source-map'),
        debug: (options.production === false),
        plugins,
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    include: /gsap/, // fix gsap
                    loader: 'imports?define=>false',
                }, {
                    test: /\.js$/,
                    include: [srcPath],
                    loader: 'babel-loader',
                    query: {
                        cacheDirectory: true,
                    },
                }, {
                    test: /\.html$/,
                    exclude: /(node_modules|vendors)/,
                    loader: 'raw-loader',
                }, {
                    test: /\.json$/,
                    exclude: /(node_modules|vendors)/,
                    loader: 'json-loader',
                },
            ],
        },
        resolve: {
            modulesDirectories: ['node_modules', options.assetsPath('src.vendors')],
        },
    };
}
