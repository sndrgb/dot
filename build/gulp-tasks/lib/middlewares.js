const historyFallback = require('connect-history-api-fallback');

module.exports = function() {
    return [
        historyFallback(),
    ];
};
